<?php

function perolehan_medali($data) {
    if( empty($data) ){
        $output = "no data";
        return $output;
    }

    $output = array(
        array(
            'negara' => 'Indonesia',
            'emas' => 0,
            'perak' => 0,
            'perunggu' => 0
        ),
        array(
            'negara' => 'India',
            'emas' => 0,
            'perak' => 0,
            'perunggu' => 0
        ),
        array(
            'negara' => 'Korea Selatan',
            'emas' => 0,
            'perak' => 0,
            'perunggu' => 0
        )
    );

    for( $i = 0; $i < count($data); $i++) {
        $negara = $data[$i];
        if( $negara[0] == "Indonesia"){
            if ( $negara[1] == 'emas'){
                $output[0]['emas'] += 1;
            } else if( $negara[1] == 'perak'){
                $output[0]['perak'] += 1;
            } else if( $negara[1] == 'perunggu'){
                $output[0]['perunggu'] += 1;
            }
        } else if ( $negara[0] == "India"){
            if ( $negara[1] == 'emas'){
                $output[1]['emas'] += 1;
            } else if( $negara[1] == 'perak'){
                $output[1]['perak'] += 1;
            } else if( $negara[1] == 'perunggu'){
                $output[1]['perunggu'] += 1;
            }
        } else if ( $negara[0] == "Korea Selatan"){
            if ( $negara[1] == 'emas'){
                $output[2]['emas'] += 1;
            } else if( $negara[1] == 'perak'){
                $output[2]['perak'] += 1;
            } else if( $negara[1] == 'perunggu'){
                $output[2]['perunggu'] += 1;
            }
        }
    }

    return $output;
}

$data = array(
    array('Indonesia', 'emas'),
    array('India', 'perak'),
    array('Korea Selatan', 'emas'),
    array('India', 'perak'),
    array('India', 'emas'),
    array('Indonesia', 'perak'),
    array('Indonesia', 'emas')
);

print_r(perolehan_medali($data));
print_r(perolehan_medali([])); //no data
?>