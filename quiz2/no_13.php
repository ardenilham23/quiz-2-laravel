<?php
$value = 0;
function hitung($string_data) {
    $penjumlahan = explode('+',$string_data);
    $perkalian = explode('*',$string_data);
    $pembagian = explode(':',$string_data);
    $modulus = explode('%',$string_data);
    $pengurangan = explode('-',$string_data);

    if( count($perkalian) >= 2) {
        $value = $perkalian[0] * $perkalian[1];
    } else if( count($penjumlahan) >= 2 ){
        $value = $penjumlahan[0] + $penjumlahan[1];
    } else if( count($pembagian) >= 2 ){
        $value = $pembagian[0] / $pembagian[1];
    } else if( count($modulus) >= 2 ){
        $value = $modulus[0] % $modulus[1];
    } else if( count($pengurangan) >=2 ){
        $value = $pengurangan[0] - $pengurangan[1];
    } 

    return $value;
}

echo hitung("102*2");
echo "<br><br>";
echo hitung("2+3");
echo "<br><br>";
echo hitung("100:25");
echo "<br><br>";
echo hitung("10%2");
echo "<br><br>";
echo hitung("99-2");
?>